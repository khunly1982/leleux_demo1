import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { exhaustMap } from 'rxjs/operators';
import { Animal } from '../models/animal';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {

  context$!: Observable<Animal[]>;

  constructor(private http: HttpClient) 
  { 
    this.context$ = interval(5000).pipe(
      exhaustMap(() => this.http.get<Animal[]>('http://localhost:52769/api/animal'))
    )
  }
}
