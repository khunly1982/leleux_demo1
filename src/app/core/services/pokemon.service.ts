import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { concat, interval, merge, Observable, Subject, timer } from 'rxjs';
import { mergeAll, mergeMap } from 'rxjs/operators';
import { Pokedex, Result } from '../models/pokedex.model';
import { Pokemon } from '../models/pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  context$: Observable<Pokedex>;

  refresh$: Subject<any>;

  selectedPokemon$: Subject<Result>

  currentUrl: string = 'https://pokeapi.co/api/v2/pokemon';

  constructor(
    private http: HttpClient
  ) { 
    this.refresh$ = new Subject<any>();
    this.selectedPokemon$ = new Subject<Result>();
    this.context$ = merge(timer(0,5000), this.refresh$)
      .pipe(mergeMap(() => this.getAll()))
  }

  public getAll() : Observable<Pokedex> {
    return this.http.get<Pokedex>(this.currentUrl)
  }

  public getDetails(url: string) : Observable<Pokemon> {
    return this.http.get<Pokemon>(url);
  }
}
