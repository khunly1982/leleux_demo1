import { AbstractControl, ValidationErrors } from "@angular/forms";

export class CustomValidators {
    static notAfterToday(control: AbstractControl) : ValidationErrors|null {
        const value : Date = control.value;
        if(value && value.getTime() > Date.now()) {
            return { 'afterToday': true }
        }
        return null;
    }
}