import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-form-error',
  templateUrl: './form-error.component.html',
  styleUrls: ['./form-error.component.scss']
})
export class FormErrorComponent implements OnInit {

  @Input()
  control!: AbstractControl | null

  error!: string | null;

  constructor() { }

  ngOnInit(): void {
    this.control?.statusChanges.subscribe(data => {
      const errors = Object.keys(this.control?.errors|| []);
      if (errors.length) {
        this.error = errors[0];
      }
    });
  }

}
