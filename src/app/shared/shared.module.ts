import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbButtonModule, NbCardModule, NbDatepickerModule, NbDialogModule, NbIconModule, NbInputModule, NbLayoutModule, NbListModule, NbMenuModule, NbSelectModule, NbSidebarModule, NbTabComponent, NbTableModule, NbToastrModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormErrorComponent } from './form-error/form-error.component';



@NgModule({
  declarations: [
    FormErrorComponent
  ],
  imports: [
    CommonModule,
    NbButtonModule,
    NbInputModule,
    NbLayoutModule,
    NbMenuModule.forRoot(),
    NbInputModule,
    NbCardModule,
    NbToastrModule.forRoot(),
    NbIconModule,
    NbSelectModule,
    NbDialogModule.forRoot(),
    NbTableModule,
    NbDatepickerModule.forRoot(),
    NbSelectModule,
    NbSidebarModule.forRoot(),
    NbEvaIconsModule,
    NbListModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    NbButtonModule,
    NbInputModule,
    NbLayoutModule,
    NbMenuModule,
    NbInputModule,
    NbCardModule,
    NbToastrModule,
    NbIconModule,
    NbSelectModule,
    NbDialogModule,
    NbTableModule,
    NbDatepickerModule,
    NbSelectModule,
    NbSidebarModule,
    NbEvaIconsModule,
    NbListModule,
    FormsModule,
    ReactiveFormsModule,
    FormErrorComponent
  ]
})
export class SharedModule { }
