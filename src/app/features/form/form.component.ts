import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/shared/validators/custom.validators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  form!: FormGroup

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      'email': [null, [
        Validators.required,
        Validators.email,
      ]],
      'nom': [null, [
        Validators.required,
        Validators.maxLength(10)
      ]],
      'date_de_naissance': [null, [
        Validators.required,
        CustomValidators.notAfterToday
      ]]
    });
  }

  onSubmit() {
    if(this.form.invalid) return;

    //call api
    console.log(this.form.value)
  }

}
