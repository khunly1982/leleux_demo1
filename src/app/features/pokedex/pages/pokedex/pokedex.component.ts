import { Component, OnInit } from '@angular/core';
import { Result } from 'src/app/core/models/pokedex.model';

@Component({
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss']
})
export class PokedexComponent implements OnInit {

  selectedResult!: Result;

  constructor() { }

  ngOnInit(): void {
  }

  onOutput(result: Result) {
    this.selectedResult = result;
  }

}
