import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Pokedex, Result } from 'src/app/core/models/pokedex.model';
import { PokemonService } from 'src/app/core/services/pokemon.service';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.scss']
})
export class LeftComponent implements OnInit {

  model!: Pokedex;

  offset = 0;

  limit = 20;

  //listPages: number[] = []; 

  @Output()
  output: EventEmitter<Result>

  constructor(
    private pokedexService: PokemonService
  ) { 
    this.output = new EventEmitter<Result>();
  }

  ngOnInit(): void {
    this.pokedexService.context$
      .subscribe(data =>  {
        this.model = data;
        // this.listPages = [];
        // for(let i=0; i< this.model.count; i+=this.limit) {
        //   this.listPages.push(i)
        // }
      });
  }

  // goto(i: number) {
  //   console.log(i);
    
  //   //this.pokedexService.currentUrl = "https://pokeapi.co/api/v2/pokemon?offset="+i
  // }

  onPreviousClick() {
    if(this.model.previous) {
      this.pokedexService.currentUrl = this.model.previous;
      this.pokedexService.refresh$.next();
    }
  }

  onNextClick() {
    console.log(this.model.next)
    if(this.model.next) {
      this.pokedexService.currentUrl = this.model.next;
      this.pokedexService.refresh$.next();
    }
  }

  onClick(result: Result) {
    //this.output.next(result);
    this.pokedexService.selectedPokemon$.next(result);
  }

}
