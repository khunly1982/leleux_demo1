import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { mergeMap } from 'rxjs/operators';
import { Result } from 'src/app/core/models/pokedex.model';
import { Pokemon } from 'src/app/core/models/pokemon';
import { PokemonService } from 'src/app/core/services/pokemon.service';

@Component({
  selector: 'app-right',
  templateUrl: './right.component.html',
  styleUrls: ['./right.component.scss']
})
export class RightComponent implements OnInit, OnChanges {

  @Input()
  input!: Result;

  model!: Pokemon;

  constructor(
    private pokeService: PokemonService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    // if(this.input) {
    //   this.pokeService.getDetails(this.input.url)
    //     .subscribe(data => this.model = data)
    // }
  }

  ngOnInit(): void {
    this.pokeService.selectedPokemon$
      .pipe(mergeMap(result => this.pokeService.getDetails(result.url)))
      .subscribe(data => this.model = data)
  }

}
