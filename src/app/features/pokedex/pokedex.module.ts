import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PokedexComponent } from './pages/pokedex/pokedex.component';
import { LeftComponent } from './components/left/left.component';
import { RightComponent } from './components/right/right.component';
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
    declarations:[
    PokedexComponent,
    LeftComponent,
    RightComponent
  ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            { path: '', component: PokedexComponent }
        ]),
        SharedModule
    ],
    exports: [],
    providers: []
})
export class PokedexModule {

}