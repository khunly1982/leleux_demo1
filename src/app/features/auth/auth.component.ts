import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  form!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authS: AuthService,
    private toastService: NbToastrService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      'email': [],
      'password': []
    });
  }

  onSubmit() {
    this.authS.login(this.form.value).subscribe(data => {
      // afficher un message de success
      this.toastService.success('Bienvenue ...');
      // redirection
      console.log(data);
    }, e => {
      this.toastService.danger(e.message)
      return of(e)
    }, () => {

    });
  }

}
