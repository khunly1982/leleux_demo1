import { ThisReceiver } from '@angular/compiler';
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { fromEvent, Observable, of, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, concatMap, distinctUntilChanged, exhaustMap, filter, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { AnimalService } from 'src/app/core/services/animal.service';
import { Animal } from 'src/app/core/models/animal';

@Component({
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements AfterViewInit, OnInit, OnDestroy {

  private _destroyed$: Subject<any>;
  // variable!: number;

  // value!: number;

  // @ViewChild('myButton')
  // button!: ElementRef;

  animals$!: Observable<Animal[]>

  constructor(
    private router: Router,
    private http: HttpClient,
    private animalService: AnimalService
  ) { 
    this._destroyed$ = new Subject<any>();
  }

  ngAfterViewInit(): void {
    // const btn =  this.button?.nativeElement;
    // fromEvent(btn, 'click').pipe(mergeMap(() => this.get()))
    //   .subscribe(data => this.variable = data)

  }

  ngOnDestroy(): void {
    this._destroyed$.next();
    this._destroyed$.complete();
  }

  ngOnInit() {
    this.animals$ = this.animalService.context$
      //.pipe(distinctUntilChanged())
    

    // this.request()
    // .pipe(
    //   takeUntil(this._destroyed$),
    //   distinctUntilChanged(),
    //   //filter(x => x === 45),
    //   mergeMap(data => this.otherRequest(data)),
    //   catchError(e => {
    //     // redirection
    //     this.router.navigateByUrl('/auth')
    //     console.log("redirection")
    //     return of(e)
    //   })
    // ).subscribe(data => console.log(data));
    
    // of(1,2,3,4,5,6,7).pipe(exhaustMap(data => this.get(data)))
    //   .subscribe(console.log)
  }

  // get() {
  //   return this.http.get<number>("http://localhost:52769/api/demo/" + this.value);
  // }

  // request() {
  //   return new Observable<number>(o => {
  //     let v = 42;
  //     setTimeout(() => v = 43, 4000);
  //     setTimeout(() => v = 44, 6000)
  //     setInterval(() => {
  //       if(v === 44) o.error()
  //       o.next(v);
  //     }, 1000)
  //   })
  // }

  // otherRequest(nb: number) {
  //   return of(nb*2);
  // }

}
