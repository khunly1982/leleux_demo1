import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { Demo1Component } from './pages/demo1/demo1.component';
import { ReservationComponent } from './pages/reservation/reservation.component';
import { StockComponent } from './pages/stock/stock.component';

const routes: Routes = [{ path: '', component: AdminComponent, children: [
  { path: 'reservations', component: ReservationComponent },
  { path: 'stock', component: StockComponent },
  { path: 'demo1', component: Demo1Component }
] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
