import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { ReservationComponent } from './pages/reservation/reservation.component';
import { StockComponent } from './pages/stock/stock.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Demo1Component } from './pages/demo1/demo1.component';


@NgModule({
  declarations: [
    AdminComponent,
    ReservationComponent,
    StockComponent,
    Demo1Component,
    
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
  ]
})
export class AdminModule { }
