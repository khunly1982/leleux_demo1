import { Component } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  items: NbMenuItem[] = [
    { title: 'Reservations',  link: '/admin/reservations', icon: 'book' },
    { title: 'Stock',  link: '/admin/stock', icon: 'star' },
    { title: 'Demo 1 - Promise / Observable',  link: '/admin/demo1', icon: 'sun' },
    { title: 'Demo 2 - Pokedex',  link: '/pokedex', icon: 'sun' },
    { title: 'Demo 3 - Forumlaire',  link: '/form', icon: 'sun' },
  ]
}
